
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author judit
 */
public class FP_Functional_Exercises {
    public static void main (String[] args){
        List<Integer> numbers = List.of(12,9,13,4,6,2,4,12,15);
        
        List<String> courses = List.of("Spring", "Spring Boot", "API","Microservices", "AWS", "PCF","Azure", "Docker", "Kubernetes");


        
        System.out.println("\n\nEjercicio1: Imprimir numeros impares");
        printNumbersInListFunctional(numbers);

	    System.out.println("\n\nEjercicio2: Imprimir los cursos");
        printCoursesInListFunctional(courses);
        
	    System.out.println("\n\nEjercicio3: Imprimir la palabra Spring de la cadena");
        printCoursesInListFunctionalSpring(courses);

        System.out.println("\n\nEjercicio4:Imprimir las palabras con mas de 4 letras");
        printAllInListFunctionalWords(courses);
        
        
	    System.out.println("\n\nEjercicio5:Imprimir el cuadrado de los numeros impares");
        printOddNumbersInListFunctional(numbers);

        System.out.println("\n\nEjercicio6:\nImprimir el numero de letras de cada palabra");
        printAllNumberOfCoursesInListFunctionalCaracter(courses);

    }

    
    private static void printNumber(int number){
        System.out.print(number + ", ");
    }
   
    private static void printString(String course){
        System.out.print(course + ", ");
    }
   
    private static boolean isEven(int number){
        return (number % 2 == 0);
    }
    
    
    
    
    

//Ejercicio1 : Imprimir numeros impares
     
    private static boolean isOdd(int number){
        return (number % 2 == 1);
    }

    private static void printNumbersInListFunctional(List<Integer> numbers){
        numbers.stream()                        
            .filter(FP_Functional_Exercises::isOdd)   
            .forEach(FP_Functional_Exercises::printNumber);
        System.out.println("");
    }
    
    
//Ejercicio 2: Imprimir la cadena
    private static void printCoursesInListFunctional(List<String> courses){
        courses.stream()                          
            .forEach(FP_Functional_Exercises::printString);
        System.out.println("");
    }



//Ejercicio3: Imprimir la cadena con la palabra Spring
    private static void printCoursesInListFunctionalSpring(List<String> courses){
        courses.stream()
            .filter(course -> course.contains("Spring"))                         
            .forEach(FP_Functional_Exercises::printString);
        System.out.println("");
    }

//Ejercicio4: Imprimir las palabras con mas de 4 letras
    private static void printAllInListFunctionalWords(List<String> courses){
        courses.stream()
            .filter(course -> 
course.length() >= 4)                         
            .forEach(FP_Functional_Exercises::printString);
        System.out.println("");
    }

//Ejercicio5: IMprimir el cadrado de los numeros impares
private static void printOddNumbersInListFunctional(List<Integer> numbers){
        numbers.stream()                        
            .filter(FP_Functional_Exercises::isOdd)
            .map(number -> number * number)   
            .forEach(FP_Functional_Exercises::printNumber);
        System.out.println("");
    }
    
    
//Ejercicio6: Imprimir el numero de letras de cada caracter
    private static void printAllNumberOfCoursesInListFunctionalCaracter (List<String> courses){
        courses.stream()
            .map(course -> course.length())                         
            .forEach(FP_Functional_Exercises::printNumber);
        System.out.println("");
    }
}
